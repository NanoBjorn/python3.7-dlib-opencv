FROM python:3.7

RUN apt-get update && apt-get install -y cmake

ENV PYTHONUNBUFFERED 1

RUN mkdir /code
WORKDIR /code
COPY . /code/
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
WORKDIR /
RUN rm -r /code
